/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fiberhome.springboot.web.rest.vm;
